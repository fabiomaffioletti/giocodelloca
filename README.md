# Environment requirements

- Java 11
- Maven 3.6.1

# How to build and run

- Clone the project
- Move to the project directory
- Run `mvn clean package`
- Run `java -jar target/giocodelloca-1.0-SNAPSHOT.jar`
