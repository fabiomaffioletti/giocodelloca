package me.fabiomaffioletti;

import me.fabiomaffioletti.matcher.AddPlayerCommandLineExecutor;
import me.fabiomaffioletti.matcher.MovePlayerCommandLineExecutor;
import me.fabiomaffioletti.matcher.RollDiceCommandLineExecutor;
import me.fabiomaffioletti.model.Game;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        var game = new Game();
        var commandLineMatchers = List.of(new AddPlayerCommandLineExecutor(), new MovePlayerCommandLineExecutor(), new RollDiceCommandLineExecutor());

        while(true) {
            System.out.print("> ");
            String input = scanner.nextLine();

            if(input.equals("quit")) {
                break;
            }

            commandLineMatchers.stream()
                    .map(commandLineMatcher -> commandLineMatcher.apply(input, game))
                    .filter(Optional::isPresent).findFirst()
                    .ifPresentOrElse(result -> System.out.println(result.get()), () -> System.out.println("Unrecognized command"));
        }

        scanner.close();
    }

}
