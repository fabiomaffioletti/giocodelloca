package me.fabiomaffioletti.exception;

public class PlayerAlreadyExistingException extends RuntimeException {

    public PlayerAlreadyExistingException(String message) {
        super(message);
    }

}
