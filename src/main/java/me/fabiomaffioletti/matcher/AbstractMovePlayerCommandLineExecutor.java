package me.fabiomaffioletti.matcher;

import me.fabiomaffioletti.exception.PlayerNotFoundException;
import me.fabiomaffioletti.model.Game;
import me.fabiomaffioletti.model.MoveContext;
import me.fabiomaffioletti.model.Player;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.of;

public abstract class AbstractMovePlayerCommandLineExecutor {

    protected Pattern pattern;

    protected Matcher matcher;

    protected Optional<String> apply(String regexp, String commandLine, Game game) {
        pattern = Pattern.compile(regexp);
        matcher = pattern.matcher(commandLine);
        if (matcher.matches()) {
            try {
                Player player = game.getPlayerByName(matcher.group(1));
                int roll1 = getFirstRoll();
                int roll2 = getSecondRoll();

                MoveContext moveContext = game.movePlayer(player.getName(), roll1, roll2);
                return moveContext.getType().getResultMessageMapper().getResultMessage(moveContext);

            } catch (PlayerNotFoundException e) {
                return of(format("%s: player not existing", e.getMessage()));
            }
        }

        return empty();
    }

    protected abstract int getFirstRoll();

    protected abstract int getSecondRoll();

}
