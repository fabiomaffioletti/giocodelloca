package me.fabiomaffioletti.matcher;

import me.fabiomaffioletti.exception.PlayerAlreadyExistingException;
import me.fabiomaffioletti.model.Game;
import me.fabiomaffioletti.model.Player;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.stream.Collectors.joining;

public class AddPlayerCommandLineExecutor implements CommandLineExecutor {

    private static final String ADD_PLAYER_COMMAND_REGEXP = "^add player (\\w+)";

    @Override
    public Optional<String> apply(String commandLine, Game game) {
        Pattern addPlayerPattern = Pattern.compile(ADD_PLAYER_COMMAND_REGEXP);
        Matcher matcher = addPlayerPattern.matcher(commandLine);
        if(matcher.matches()) {
            try {
                game.addPlayer(matcher.group(1));
                return of(String.format("players: %s", game.getPlayers().stream().map(Player::getName).collect(joining(", "))));
            } catch (PlayerAlreadyExistingException e) {
                return of(String.format("%s: already existing player", e.getMessage()));
            }
        }

        return empty();
    }

}
