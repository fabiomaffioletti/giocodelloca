package me.fabiomaffioletti.matcher;

import me.fabiomaffioletti.model.Game;

import java.util.Optional;

public interface CommandLineExecutor {

    Optional<String> apply(String commandLine, Game game);

}
