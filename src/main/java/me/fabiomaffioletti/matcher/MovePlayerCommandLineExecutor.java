package me.fabiomaffioletti.matcher;

import me.fabiomaffioletti.model.Game;

import java.util.Optional;

import static java.lang.Integer.parseInt;

public class MovePlayerCommandLineExecutor extends AbstractMovePlayerCommandLineExecutor implements CommandLineExecutor {

    private static final String MOVE_PLAYER_COMMAND_REGEXP = "^move (\\w+) (\\d{1}), (\\d{1})$";

    @Override
    public Optional<String> apply(String commandLine, Game game) {
        return apply(MOVE_PLAYER_COMMAND_REGEXP, commandLine, game);
    }

    @Override
    protected int getFirstRoll() {
        return parseInt(matcher.group(2));
    }

    @Override
    protected int getSecondRoll() {
        return parseInt(matcher.group(3));
    }

}
