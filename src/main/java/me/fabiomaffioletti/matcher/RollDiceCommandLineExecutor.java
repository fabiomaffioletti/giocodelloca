package me.fabiomaffioletti.matcher;

import me.fabiomaffioletti.model.Dice;
import me.fabiomaffioletti.model.Game;

import java.util.Optional;

public class RollDiceCommandLineExecutor extends AbstractMovePlayerCommandLineExecutor implements CommandLineExecutor {

    private static final String ROLL_DICE_AND_MOVE_PLAYER_COMMAND_REGEXP = "^move (\\w+)$";

    private Dice dice;

    public RollDiceCommandLineExecutor() {
        dice = new Dice();
    }

    @Override
    public Optional<String> apply(String commandLine, Game game) {
        return apply(ROLL_DICE_AND_MOVE_PLAYER_COMMAND_REGEXP, commandLine, game);
    }

    @Override
    protected int getFirstRoll() {
        return dice.roll();
    }

    @Override
    protected int getSecondRoll() {
        return dice.roll();
    }

}
