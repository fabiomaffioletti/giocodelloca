package me.fabiomaffioletti.model;

import java.util.Random;

public class Dice {

    private Random random;

    public Dice() {
        random = new Random();
    }

    public Integer roll() {
        return random.nextInt(6) + 1;
    }

}
