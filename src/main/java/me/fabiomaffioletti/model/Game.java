package me.fabiomaffioletti.model;

import lombok.Getter;
import me.fabiomaffioletti.exception.PlayerAlreadyExistingException;
import me.fabiomaffioletti.exception.PlayerNotFoundException;
import me.fabiomaffioletti.model.MoveContext.MoveType;

import java.util.ArrayList;
import java.util.List;

import static me.fabiomaffioletti.model.MoveContext.MoveType.*;
import static me.fabiomaffioletti.util.PositionUtil.THE_BRIDGE_TARGET;
import static me.fabiomaffioletti.util.PositionUtil.WIN_SCORE;

@Getter
public class Game {

    private List<Player> players;

    public Game() {
        players = new ArrayList<>();
    }

    public void addPlayer(String playerName) throws PlayerAlreadyExistingException {
        Player player = Player.builder().name(playerName).position(0).build();
        if (players.contains(player)) {
            throw new PlayerAlreadyExistingException(playerName);
        } else {
            this.players.add(player);
        }
    }

    public MoveContext movePlayer(String playerName, int roll1, int roll2) {
        Player player = getPlayerByName(playerName);
        int previousPosition = player.getPosition();
        int amount = roll1 + roll2;

        if (player.bridges(roll1, roll2)) {
            player.modifyPosition(THE_BRIDGE_TARGET);
            return moveContextBuilder(BRIDGE, player, previousPosition, roll1, roll2).build();
        }

        player.move(roll1 + roll2);
        var jumpContext = JumpContext.builder().jumpPositions(new ArrayList<>()).build();
        jumpContext.addJumpPosition(player.getPosition());

        if (player.bounces()) {
            player.modifyPosition(WIN_SCORE - (player.getPosition() - WIN_SCORE));
            return moveContextBuilder(BOUNCE, player, previousPosition, roll1, roll2).build();
        }

        if (player.jumps()) {
            player.move(amount);
            jumpContext.addJumpPosition(player.getPosition());

            while(player.jumps()) {
                player.move(amount);
                jumpContext.addJumpPosition(player.getPosition());
            }

            return moveContextBuilder(GOOSE, player, previousPosition, roll1, roll2).jumpContext(jumpContext).build();
        }

        if(player.wins()) {
            return moveContextBuilder(WINNER, player, previousPosition, roll1, roll2).build();
        }

        return moveContextBuilder(STANDARD, player, previousPosition, roll1, roll2).build();
    }

    private MoveContext.MoveContextBuilder moveContextBuilder(MoveType moveType, Player player, int previousPosition, int roll1, int roll2) {
        return MoveContext.builder().type(moveType).player(player).previousPosition(previousPosition).roll1(roll1).roll2(roll2);
    }

    public Player getPlayerByName(String playerName) {
        return this.players.stream().filter(player -> player.getName().equals(playerName)).findFirst().orElseThrow(() -> new PlayerNotFoundException(playerName));
    }

}
