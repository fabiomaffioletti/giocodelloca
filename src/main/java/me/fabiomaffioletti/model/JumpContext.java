package me.fabiomaffioletti.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class JumpContext {

    private List<Integer> jumpPositions;

    public void addJumpPosition(Integer position) {
        this.jumpPositions.add(position);
    }

}
