package me.fabiomaffioletti.model;

import lombok.Builder;
import lombok.Getter;
import me.fabiomaffioletti.result.*;

@Getter
@Builder
public class MoveContext {

    private MoveType type;

    private Player player;

    private int previousPosition;

    private int roll1;

    private int roll2;

    private JumpContext jumpContext;

    public enum MoveType {
        STANDARD(new StandardResultMessageMapper()),
        WINNER(new WinnerResultMessageMapper()),
        BOUNCE(new BounceResultMessageMapper()),
        BRIDGE(new BridgeResultMessageMapper()),
        GOOSE(new GooseResultMessageMapper());

        private ResultMessageMapper resultMessageMapper;

        MoveType(ResultMessageMapper resultMessageMapper) {
            this.resultMessageMapper = resultMessageMapper;
        }

        public ResultMessageMapper getResultMessageMapper() {
            return resultMessageMapper;
        }
    }

}
