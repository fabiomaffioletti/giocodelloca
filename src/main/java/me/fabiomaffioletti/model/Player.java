package me.fabiomaffioletti.model;

import lombok.*;
import me.fabiomaffioletti.util.PositionUtil;

import static me.fabiomaffioletti.util.PositionUtil.THE_BRIDGE_SOURCE;
import static me.fabiomaffioletti.util.PositionUtil.WIN_SCORE;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "name")
public class Player {

    private String name;

    private int position;

    public void move(int amount) {
        position += amount;
    }

    public void modifyPosition(int position) {
        this.position = position;
    }

    protected boolean bounces() {
        return getPosition() > WIN_SCORE;
    }

    protected boolean jumps() {
        return PositionUtil.of(getPosition()).equals(PositionUtil.Position.GOOSE);
    }

    protected boolean wins() {
        return getPosition() == WIN_SCORE;
    }

    protected boolean bridges(int roll1, int roll2) {
        return getPosition() == THE_BRIDGE_SOURCE && roll1 == roll2 && roll1 == 1;
    }

}
