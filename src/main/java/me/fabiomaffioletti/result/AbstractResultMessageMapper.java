package me.fabiomaffioletti.result;

import me.fabiomaffioletti.model.MoveContext;
import me.fabiomaffioletti.model.Player;
import me.fabiomaffioletti.util.PositionUtil;

import java.util.Optional;

import static java.lang.String.format;
import static java.lang.String.join;
import static java.util.Optional.of;

public abstract class AbstractResultMessageMapper {
    final String MOVE_MESSAGE_PLACEHOLDER = "%s rolls %s, %s. %s moves from %s to %s";

    public Optional<String> getResultMessage(MoveContext moveContext) {
        Player player = moveContext.getPlayer();
        String moveResultString = format(
                MOVE_MESSAGE_PLACEHOLDER,
                player.getName(),
                moveContext.getRoll1(),
                moveContext.getRoll2(),
                player.getName(),
                PositionUtil.format(moveContext.getPreviousPosition()),
                getSpecificPositionLabel(moveContext));
        return of(join(". ", moveResultString, getAdditionalResultMessage(moveContext)).trim());
    }

    protected abstract String getSpecificPositionLabel(MoveContext moveContext);

    protected abstract String getAdditionalResultMessage(MoveContext moveContext);

}
