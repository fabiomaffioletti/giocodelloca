package me.fabiomaffioletti.result;

import me.fabiomaffioletti.model.MoveContext;

import static java.lang.String.format;
import static me.fabiomaffioletti.util.PositionUtil.WIN_SCORE;
import static me.fabiomaffioletti.util.PositionUtil.format;

public class BounceResultMessageMapper extends AbstractResultMessageMapper implements ResultMessageMapper {

    @Override
    protected String getSpecificPositionLabel(MoveContext moveContext) {
        return String.valueOf(WIN_SCORE);
    }

    @Override
    protected String getAdditionalResultMessage(MoveContext moveContext) {
        final String BOUNCE_MESSAGE_PLACEHOLDER = "%s bounces! %s returns to %s";
        return format(BOUNCE_MESSAGE_PLACEHOLDER, moveContext.getPlayer().getName(), moveContext.getPlayer().getName(), format(moveContext.getPlayer().getPosition()));
    }

}
