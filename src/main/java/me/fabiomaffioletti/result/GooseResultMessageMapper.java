package me.fabiomaffioletti.result;

import me.fabiomaffioletti.model.MoveContext;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
import static java.lang.String.join;
import static me.fabiomaffioletti.util.PositionUtil.format;

public class GooseResultMessageMapper extends AbstractResultMessageMapper implements ResultMessageMapper {

    @Override
    protected String getSpecificPositionLabel(MoveContext moveContext) {
        return format(moveContext.getJumpContext().getJumpPositions().get(0));
    }

    @Override
    protected String getAdditionalResultMessage(MoveContext moveContext) {
        List<Integer> jumpPositions = moveContext.getJumpContext().getJumpPositions();
        var gooses = new ArrayList<String>();
        for (int i = 1; i <= jumpPositions.size() - 1; i++) {
            final String GOOOSE_MESSAGE_PLACEHOLDER = "%s moves again and goes to %s";
            gooses.add(format(GOOOSE_MESSAGE_PLACEHOLDER, moveContext.getPlayer().getName(), format(jumpPositions.get(i))));
        }

        return join(". ", gooses);
    }

}
