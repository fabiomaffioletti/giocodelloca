package me.fabiomaffioletti.result;

import me.fabiomaffioletti.model.MoveContext;

import java.util.Optional;

public interface ResultMessageMapper {

    Optional<String> getResultMessage(MoveContext moveContext);

}
