package me.fabiomaffioletti.result;

import me.fabiomaffioletti.model.MoveContext;

import static me.fabiomaffioletti.util.PositionUtil.format;

public class StandardResultMessageMapper extends AbstractResultMessageMapper implements ResultMessageMapper {

    @Override
    protected String getSpecificPositionLabel(MoveContext moveContext) {
        return format(moveContext.getPlayer().getPosition());
    }

    @Override
    protected String getAdditionalResultMessage(MoveContext moveContext) {
        return "";
    }

}
