package me.fabiomaffioletti.result;

import me.fabiomaffioletti.model.MoveContext;

import static java.lang.String.format;
import static me.fabiomaffioletti.util.PositionUtil.format;

public class WinnerResultMessageMapper extends AbstractResultMessageMapper implements ResultMessageMapper {

    @Override
    protected String getSpecificPositionLabel(MoveContext moveContext) {
        return format(moveContext.getPlayer().getPosition());
    }

    @Override
    protected String getAdditionalResultMessage(MoveContext moveContext) {
        final String WINNER_MESSAGE_PLACEHOLDER = "%s Wins!!";
        return format(WINNER_MESSAGE_PLACEHOLDER, moveContext.getPlayer().getName());
    }

}
