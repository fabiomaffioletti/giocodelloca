package me.fabiomaffioletti.util;

import java.util.Map;

import static me.fabiomaffioletti.util.PositionUtil.Position.*;

public class PositionUtil {

    public final static int WIN_SCORE = 63;

    public final static int THE_BRIDGE_SOURCE = 4;

    public final static int THE_BRIDGE_TARGET = 12;

    private static Map<Integer, Position> positions = Map.of(
            0, START,
            12, BRIDGE,
            5, GOOSE,
            9, GOOSE,
            14, GOOSE,
            18, GOOSE,
            23, GOOSE,
            27, GOOSE
    );

    public static String format(int position) {
        switch (positions.getOrDefault(position, DEFAULT)) {
            case START:
                return "Start";
            case BRIDGE:
                return "The Bridge";
            case GOOSE:
                return String.format("%s, The Goose", position);
            case DEFAULT:
            default:
                return String.valueOf(position);
        }
    }

    public static Position of(int position) {
        return positions.getOrDefault(position, DEFAULT);
    }

    public enum Position {
        START, BRIDGE, GOOSE, DEFAULT
    }

}
