package me.fabiomaffioletti.matcher;

import me.fabiomaffioletti.model.Game;
import org.junit.Test;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class AddPlayerCommandLineExecutorTest {

    @Test
    public void itShouldAddAPlayerToAGameWithSuccess() {
        CommandLineExecutor commandLineExecutor = new AddPlayerCommandLineExecutor();
        Game game = new Game();
        commandLineExecutor.apply("add player pippo", game);
        assertThat(game.getPlayers(), hasSize(1));
    }

}