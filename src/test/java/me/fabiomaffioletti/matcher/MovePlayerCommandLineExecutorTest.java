package me.fabiomaffioletti.matcher;

import me.fabiomaffioletti.model.Game;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class MovePlayerCommandLineExecutorTest {

    @Test
    public void itShouldMoveAPlayerWithSuccess() {
        var commandLineMatcher = new MovePlayerCommandLineExecutor();
        var game = new Game();
        game.addPlayer("pippo");
        Optional<String> result = commandLineMatcher.apply("move pippo 4, 2", game);
        assertThat(game.getPlayerByName("pippo").getPosition(), is(6));
        assertEquals("pippo rolls 4, 2. pippo moves from Start to 6.", result.get());
    }

    @Test
    public void itShouldMoveAPlayerAndDisplayWinnerWithSuccess() {
        var commandLineMatcher = new MovePlayerCommandLineExecutor();
        var game = new Game();
        game.addPlayer("pippo");
        commandLineMatcher.apply("move pippo 5, 5", game);
        commandLineMatcher.apply("move pippo 5, 5", game);
        commandLineMatcher.apply("move pippo 5, 5", game);
        commandLineMatcher.apply("move pippo 5, 5", game);
        commandLineMatcher.apply("move pippo 5, 5", game);
        commandLineMatcher.apply("move pippo 5, 5", game);
        Optional<String> result = commandLineMatcher.apply("move pippo 1, 2", game);
        assertThat(game.getPlayerByName("pippo").getPosition(), is(63));
        assertEquals("pippo rolls 1, 2. pippo moves from 60 to 63. pippo Wins!!", result.get());
    }

    @Test
    public void itShouldMoveAPlayerAndBounceHimWhenHeScoresMoreThan63() {
        var commandLineMatcher = new MovePlayerCommandLineExecutor();
        var game = new Game();
        game.addPlayer("pippo");
        commandLineMatcher.apply("move pippo 5, 5", game);
        commandLineMatcher.apply("move pippo 5, 5", game);
        commandLineMatcher.apply("move pippo 5, 5", game);
        commandLineMatcher.apply("move pippo 5, 5", game);
        commandLineMatcher.apply("move pippo 5, 5", game);
        commandLineMatcher.apply("move pippo 5, 5", game);
        Optional<String> result = commandLineMatcher.apply("move pippo 3, 2", game);
        assertThat(game.getPlayerByName("pippo").getPosition(), is(61));
        assertEquals("pippo rolls 3, 2. pippo moves from 60 to 63. pippo bounces! pippo returns to 61", result.get());
    }

    @Test
    public void itShouldMoveAPlayerUsingTheBridge() {
        var commandLineMatcher = new MovePlayerCommandLineExecutor();
        var game = new Game();
        game.addPlayer("pippo");
        commandLineMatcher.apply("move pippo 1, 3", game);
        Optional<String> result = commandLineMatcher.apply("move pippo 1, 1", game);
        assertThat(game.getPlayerByName("pippo").getPosition(), is(12));
        assertEquals("pippo rolls 1, 1. pippo moves from 4 to The Bridge. pippo jumps to 12", result.get());
    }

    @Test
    public void itShouldMoveAPlayerUsingTheSingleGoose() {
        var commandLineMatcher = new MovePlayerCommandLineExecutor();
        var game = new Game();
        game.addPlayer("pippo");
        commandLineMatcher.apply("move pippo 1, 2", game);
        Optional<String> result = commandLineMatcher.apply("move pippo 1, 1", game);
        assertThat(game.getPlayerByName("pippo").getPosition(), is(7));
        assertEquals("pippo rolls 1, 1. pippo moves from 3 to 5, The Goose. pippo moves again and goes to 7", result.get());
    }

    @Test
    public void itShouldMoveAPlayerUsingTheMultipleGoose() {
        var commandLineMatcher = new MovePlayerCommandLineExecutor();
        var game = new Game();
        game.addPlayer("pippo");
        commandLineMatcher.apply("move pippo 5, 5", game);
        Optional<String> result = commandLineMatcher.apply("move pippo 2, 2", game);
        assertThat(game.getPlayerByName("pippo").getPosition(), is(22));
        assertEquals("pippo rolls 2, 2. pippo moves from 10 to 14, The Goose. pippo moves again and goes to 18, The Goose. pippo moves again and goes to 22", result.get());
    }

    @Test
    public void itShouldThrowAnExceptionIfThePlayerIsUnexisting() {
        var commandLineMatcher = new MovePlayerCommandLineExecutor();
        var game = new Game();
        game.addPlayer("pippo");
        Optional<String> result = commandLineMatcher.apply("move pluto 4, 2", game);
        assertEquals("pluto: player not existing", result.get());
    }

}