package me.fabiomaffioletti.matcher;

import me.fabiomaffioletti.model.Game;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class RollDiceCommandLineExecutorTest {

    @Test
    public void itShouldRollDiceAndMoveAPlayerWithSuccess() {
        var commandLineMatcher = new RollDiceCommandLineExecutor();
        var game = new Game();
        game.addPlayer("pippo");
        Optional<String> result = commandLineMatcher.apply("move pippo", game);
        assertThat(game.getPlayerByName("pippo").getPosition(), is(greaterThanOrEqualTo(2)));
        System.out.println(result.get());
    }

}