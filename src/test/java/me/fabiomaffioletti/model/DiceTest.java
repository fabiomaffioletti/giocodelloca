package me.fabiomaffioletti.model;

import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class DiceTest {

    @Test
    public void itShouldRollAndGetARandomNumber() {
        Dice dice = new Dice();
        Integer result = dice.roll();
        assertNotNull(result);
        assertThat(result, is(lessThanOrEqualTo(6)));
        assertThat(result, is(greaterThanOrEqualTo(1)));
    }

}