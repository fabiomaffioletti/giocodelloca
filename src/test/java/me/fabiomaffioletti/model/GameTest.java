package me.fabiomaffioletti.model;

import me.fabiomaffioletti.exception.PlayerAlreadyExistingException;
import me.fabiomaffioletti.exception.PlayerNotFoundException;
import org.junit.Test;

import static me.fabiomaffioletti.model.MoveContext.MoveType.BOUNCE;
import static me.fabiomaffioletti.model.MoveContext.MoveType.WINNER;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class GameTest {

    @Test
    public void itShouldAddTwoPlayersWithSuccess() {
        var game = new Game();
        game.addPlayer("one");
        game.addPlayer("two");
        assertThat(game.getPlayers(), hasSize(2));
    }

    @Test(expected = PlayerAlreadyExistingException.class)
    public void itShouldThrowAnExceptionWhenTwoPlayersWithTheSameNameAreAdded() {
        var game = new Game();
        game.addPlayer("one");
        game.addPlayer("one");
        fail();
    }

    @Test
    public void itShouldGetAPlayerByNameWithSuccess() {
        var game = new Game();
        game.addPlayer("one");
        Player one = game.getPlayerByName("one");
        assertNotNull(one);
    }

    @Test(expected = PlayerNotFoundException.class)
    public void itShouldThrowAnExceptionWhenGettingAnUnexistingPlayerByName() {
        var game = new Game();
        game.addPlayer("one");
        game.getPlayerByName("two");
        fail();
    }

    @Test
    public void itShouldThrowAnExceptionWhenPlayerBounces() {
        var game = new Game();
        game.addPlayer("one");
        MoveContext moveContext = game.movePlayer("one", 30, 40);
        assertEquals(BOUNCE, moveContext.getType());
    }

    @Test
    public void itShouldMoveAPlayerWithSuccess() {
        var game = new Game();
        game.addPlayer("one");
        game.addPlayer("two");
        assertThat(game.getPlayerByName("one").getPosition(), is(0));

        game.movePlayer("one", 1, 2);
        assertThat(game.getPlayerByName("one").getPosition(), is(3));
    }

    @Test
    public void itShouldReturnTheWinnerPlayerIfHisPositionIs63() {
        var game = new Game();
        game.addPlayer("pippo");
        MoveContext moveContext = game.movePlayer("pippo", 62, 1);
        assertEquals(moveContext.getType(), WINNER);
    }

    @Test
    public void itShouldReturnAnEmptyOptionalIfThePositionOfPlayersIsNot63() {
        var game = new Game();
        game.addPlayer("pippo");
        MoveContext moveContext = game.movePlayer("pippo", 1, 1);
        assertNotEquals(moveContext.getType(), WINNER);
    }

}