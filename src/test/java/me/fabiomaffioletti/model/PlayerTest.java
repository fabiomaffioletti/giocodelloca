package me.fabiomaffioletti.model;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class PlayerTest {

    @Test
    public void twoPlayersWithTheSameNameShouldBeEqual() {
        var one = Player.builder().name("abc").build();
        var two = Player.builder().name("abc").build();
        assertEquals(one, two);
    }

    @Test
    public void twoPlayersWithDifferentNamesShouldBeNotEqual() {
        var one = Player.builder().name("abc").build();
        var two = Player.builder().name("def").build();
        assertNotEquals(one, two);
    }

    @Test
    public void itShouldMoveThePlayerToANewPosition() {
        var player = Player.builder().name("test").position(0).build();
        assertThat(player.getPosition(), is(0));
        player.move(3);
        assertThat(player.getPosition(), is(3));
        player.move(8);
        assertThat(player.getPosition(), is(11));
    }

    @Test
    public void itShouldBounce() {
        var player = Player.builder().position(80).build();
        assertTrue(player.bounces());
    }

    @Test
    public void itShouldWin() {
        var player = Player.builder().position(63).build();
        assertTrue(player.wins());
    }

    @Test
    public void itShouldBridge() {
        var player = Player.builder().position(4).build();
        assertTrue(player.bridges(1, 1));
    }

    @Test
    public void itShouldJump() {
        var player = Player.builder().position(5).build();
        assertTrue(player.jumps());

        player = Player.builder().position(14).build();
        assertTrue(player.jumps());
    }

}