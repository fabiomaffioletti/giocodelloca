package me.fabiomaffioletti.result;

import me.fabiomaffioletti.model.MoveContext;
import me.fabiomaffioletti.model.Player;
import org.junit.Test;

import static me.fabiomaffioletti.model.MoveContext.MoveType.BOUNCE;
import static me.fabiomaffioletti.model.MoveContext.MoveType.BRIDGE;
import static org.junit.Assert.assertEquals;

public class BounceResultMessageMapperTest {

    private BounceResultMessageMapper mapper = new BounceResultMessageMapper();

    private MoveContext moveContext = MoveContext.builder().type(BOUNCE).player(Player.builder().name("test").position(61).build()).previousPosition(60).roll1(3).roll2(2).build();

    @Test
    public void testGetSpecificPositionLabel() {
        String specificPositionLabel = mapper.getSpecificPositionLabel(moveContext);
        assertEquals("63", specificPositionLabel);
    }

    @Test
    public void testGetAdditionalResultMessage() {
        String additionalResultMessage = mapper.getAdditionalResultMessage(moveContext);
        assertEquals("test bounces! test returns to 61", additionalResultMessage);
    }

}