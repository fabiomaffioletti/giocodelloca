package me.fabiomaffioletti.result;

import me.fabiomaffioletti.model.MoveContext;
import me.fabiomaffioletti.model.Player;
import org.junit.Test;

import static me.fabiomaffioletti.model.MoveContext.MoveType.BRIDGE;
import static me.fabiomaffioletti.model.MoveContext.MoveType.WINNER;
import static org.junit.Assert.assertEquals;

public class BridgeResultMessageMapperTest {

    private BridgeResultMessageMapper mapper = new BridgeResultMessageMapper();

    private MoveContext moveContext = MoveContext.builder().type(BRIDGE).player(Player.builder().name("test").position(12).build()).previousPosition(4).roll1(1).roll2(2).build();

    @Test
    public void testGetSpecificPositionLabel() {
        String specificPositionLabel = mapper.getSpecificPositionLabel(moveContext);
        assertEquals("The Bridge", specificPositionLabel);
    }

    @Test
    public void testGetAdditionalResultMessage() {
        String additionalResultMessage = mapper.getAdditionalResultMessage(moveContext);
        assertEquals("test jumps to 12", additionalResultMessage);
    }

}