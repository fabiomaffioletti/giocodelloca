package me.fabiomaffioletti.result;

import me.fabiomaffioletti.model.JumpContext;
import me.fabiomaffioletti.model.MoveContext;
import me.fabiomaffioletti.model.Player;
import org.junit.Test;

import java.util.List;

import static me.fabiomaffioletti.model.MoveContext.MoveType.GOOSE;
import static org.junit.Assert.assertEquals;

public class MultipleGooseResultMessageMapperTest {

    private GooseResultMessageMapper mapper = new GooseResultMessageMapper();

    private JumpContext jumpContext = JumpContext.builder().jumpPositions(List.of(14, 18)).build();
    private MoveContext moveContext = MoveContext.builder().type(GOOSE).player(Player.builder().name("test").position(22).build()).previousPosition(10).roll1(2).roll2(2).jumpContext(jumpContext).build();

    @Test
    public void testGetSpecificPositionLabel() {
        String specificPositionLabel = mapper.getSpecificPositionLabel(moveContext);
        assertEquals("14, The Goose", specificPositionLabel);
    }

    @Test
    public void testGetAdditionalResultMessage() {
        String additionalResultMessage = mapper.getAdditionalResultMessage(moveContext);
        assertEquals("test moves again and goes to 18, The Goose", additionalResultMessage);
    }

}