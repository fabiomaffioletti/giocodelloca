package me.fabiomaffioletti.result;

import me.fabiomaffioletti.model.JumpContext;
import me.fabiomaffioletti.model.MoveContext;
import me.fabiomaffioletti.model.Player;
import org.junit.Test;

import java.util.List;

import static me.fabiomaffioletti.model.MoveContext.MoveType.GOOSE;
import static me.fabiomaffioletti.model.MoveContext.MoveType.STANDARD;
import static org.junit.Assert.assertEquals;

public class SingleGooseResultMessageMapperTest {

    private GooseResultMessageMapper mapper = new GooseResultMessageMapper();

    private JumpContext jumpContext = JumpContext.builder().jumpPositions(List.of(7)).build();
    private MoveContext moveContext = MoveContext.builder().type(GOOSE).player(Player.builder().name("test").position(7).build()).previousPosition(5).roll1(1).roll2(1).jumpContext(jumpContext).build();

    @Test
    public void testGetSpecificPositionLabel() {
        String specificPositionLabel = mapper.getSpecificPositionLabel(moveContext);
        assertEquals("7", specificPositionLabel);
    }

    @Test
    public void testGetAdditionalResultMessage() {
        String additionalResultMessage = mapper.getAdditionalResultMessage(moveContext);
        assertEquals("", additionalResultMessage);
    }

}