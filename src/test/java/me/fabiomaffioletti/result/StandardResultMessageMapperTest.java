package me.fabiomaffioletti.result;

import me.fabiomaffioletti.model.MoveContext;
import me.fabiomaffioletti.model.Player;
import org.junit.Test;

import static me.fabiomaffioletti.model.MoveContext.MoveType.STANDARD;
import static me.fabiomaffioletti.model.MoveContext.MoveType.WINNER;
import static org.junit.Assert.assertEquals;

public class StandardResultMessageMapperTest {

    private StandardResultMessageMapper mapper = new StandardResultMessageMapper();

    private MoveContext moveContext = MoveContext.builder().type(STANDARD).player(Player.builder().name("test").position(6).build()).previousPosition(3).roll1(1).roll2(2).build();

    @Test
    public void testGetSpecificPositionLabel() {
        String specificPositionLabel = mapper.getSpecificPositionLabel(moveContext);
        assertEquals("6", specificPositionLabel);
    }

    @Test
    public void testGetAdditionalResultMessage() {
        String additionalResultMessage = mapper.getAdditionalResultMessage(moveContext);
        assertEquals("", additionalResultMessage);
    }

}