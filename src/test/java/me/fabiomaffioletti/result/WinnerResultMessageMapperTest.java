package me.fabiomaffioletti.result;

import me.fabiomaffioletti.model.MoveContext;
import me.fabiomaffioletti.model.Player;
import org.junit.Test;

import static me.fabiomaffioletti.model.MoveContext.MoveType.WINNER;
import static org.junit.Assert.assertEquals;

public class WinnerResultMessageMapperTest {

    private WinnerResultMessageMapper mapper = new WinnerResultMessageMapper();

    private MoveContext moveContext = MoveContext.builder().type(WINNER).player(Player.builder().name("test").position(63).build()).previousPosition(60).roll1(1).roll2(2).build();

    @Test
    public void testGetSpecificPositionLabel() {
        String specificPositionLabel = mapper.getSpecificPositionLabel(moveContext);
        assertEquals("63", specificPositionLabel);
    }

    @Test
    public void testGetAdditionalResultMessage() {
        String additionalResultMessage = mapper.getAdditionalResultMessage(moveContext);
        assertEquals("test Wins!!", additionalResultMessage);
    }

}