package me.fabiomaffioletti.util;

import org.junit.Test;

import static me.fabiomaffioletti.util.PositionUtil.Position.*;
import static org.junit.Assert.assertEquals;

public class PositionUtilTest {

    @Test
    public void format() {
        assertEquals("Start", PositionUtil.format(0));
        assertEquals("The Bridge", PositionUtil.format(12));
        assertEquals("14, The Goose", PositionUtil.format(14));

    }

    @Test
    public void of() {
        assertEquals(START, PositionUtil.of(0));
        assertEquals(BRIDGE, PositionUtil.of(12));
        assertEquals(GOOSE, PositionUtil.of(5));
        assertEquals(DEFAULT, PositionUtil.of(3));
    }

}